<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" 
   "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title>Libreapps</title>
<link rel="stylesheet" type="text/css" href="<?php echo $webloc ?>/style.css" />
<!--[if lt IE 8 ]>
<link rel="stylesheet" type="text/css" href="<?php echo $webloc ?>/style_ie.css" />
<![endif]-->
<meta name="description" content="Libreapps provide free software web applications - web services that respect your freedom over the software and your own data.">
<meta name="keywords" content="libreapps, libre apps, libre, apps, free, free software, software, web, application, app, services, SAAS, freedom"> 
</head>
<body>
