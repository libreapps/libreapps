--
-- Database: `libreapps`
--

-- --------------------------------------------------------

--
-- Table structure for table `libreapps_alpha`
--

CREATE TABLE IF NOT EXISTS `libreapps_alpha` (
  `token` varchar(10) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `libreapps_users`
--

CREATE TABLE IF NOT EXISTS `libreapps_users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

