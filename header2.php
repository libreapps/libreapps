<div id="la-header">
    <div id="la-h1">
    <div id="la-logogroup"><h1><a href="<?php echo $webloc ?>/"><img id="la-logo" src="<?php echo $webloc ?>/logo.png" alt="libreapps" /></a></h1>
    <?php if ($appname) { ?>
    <h1 id="la-appname"><?php echo $appname ?></h1>
    <?php } else { ?>
    <!--<span id="la-slogan"><a href="http://dev.libreapps.com/wiki/Slogan" class="unspecial">[Insert clever slogan here]</a></span>-->
    <?php }
    echo "</div></div><div id=\"la-h2\">";
	if ($appname == "RSS"){
		echo "<div id=\"la-applist\"><a href=\"$webloc/mail\">Mail</a></div>";
	}
	elseif ($appname == "Mail"){
		echo "<div id=\"la-applist\"><a href=\"$webloc/rss\">RSS</a></div>";
	}
	else{ 
		# Change this so we take the current app out of a list rather then make a list for each app!
		echo "<div id=\"la-applist\"><a href=\"$webloc/rss\">RSS</a> | <a href=\"$webloc/mail\">Mail</a></div>";
	}
	if (!$la_username) $la_username = $_SESSION['la-username'];
	echo "</div><div id=\"la-h3\"><div id=\"la-userbox\">";
	if ($la_username && $_GET['do'] != "logout") {
    echo $la_username;
    if ($appname == "RSS") {
    	if ($pref) echo " | <a href=\"#\" onclick=\"gotoMain()\">".__('Exit preferences')."</a>";
    	else echo " | <a href=\"prefs.php\">App Preferences</a>";
	}
    if ($appname == "RSS") $logout = "logout.php";
    else $logout = "?do=logout";
?>
    | <a href="<?php echo $logout; ?>">Logout</a>
    <?php } else {
    if ($appname) $extra = "?app=".strtolower($appname); ?>
    <a href="<?php echo $webloc ?>/login.php<?php echo $extra ?>">Login</a>
    <?php } ?>
    </div></div>
</div>
